package pt.bring.bank_ws.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.npti.npti_fw.model.business.AbstractBusiness;

import pt.bring.bank_model.model.business.AccountBusiness;
import pt.bring.bank_model.model.entity.AccountEntity;

@Component
@Path("account")
public class AccountWs extends AbstractWs<AccountEntity> {

	@Autowired
	@Qualifier("accountBusinessImpl")
	private transient AccountBusiness business;

	@Override
	public AbstractBusiness<AccountEntity> getBusiness() {
		return business;
	}

	public void setBusiness(AccountBusiness business) {
		this.business = business;
	}

	private static List<AccountEntity> list = new ArrayList<>();

	/**
	 * 
	 */

	@GET
	@Path(value = "/amount/{type}")
	@Produces(value = JSON)
	public BigDecimal amount(@PathParam("type") String type) {
		try {
			return this.business.amount(type);
		} catch (Exception e) {
			this.getLogger().error(":: [" + this.getClass().getSimpleName() + "]", e.getMessage());

			return null;
		} finally {

		}
	}

	@GET
	@Path(value = "/list/")
	@Produces(value = JSON)
	public List<AccountEntity> list() {
		try {
			return this.business.list();
		} catch (Exception e) {
			this.getLogger().error(":: [" + this.getClass().getSimpleName() + "]", e.getMessage());

			return null;
		} finally {

		}
	}

	@GET
	@Path(value = "/search/{id}")
	@Produces(value = JSON)
	public List<AccountEntity> search(@PathParam("id") Long id) {
		try {
			return this.business.search(id);
		} catch (Exception e) {
			this.getLogger().error(":: [" + this.getClass().getSimpleName() + "]", e.getMessage());

			return null;
		} finally {

		}
	}

}