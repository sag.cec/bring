package pt.bring.bank_ws.view;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.npti.npti_fw.model.business.AbstractBusiness;
import com.npti.npti_fw.model.entity.AbstractEntity;

public abstract class AbstractWs<T extends AbstractEntity<? extends Serializable>> {

	private static final String CHARSET_UTF8 = "; charset=UTF-8";

	public static final String HTML = MediaType.TEXT_HTML + CHARSET_UTF8;

	public static final String TEXT = MediaType.TEXT_PLAIN + CHARSET_UTF8;

	public static final String XML = MediaType.APPLICATION_XML + CHARSET_UTF8;

	public static final String JSON = MediaType.APPLICATION_JSON + CHARSET_UTF8;

	public abstract AbstractBusiness<T> getBusiness();

	protected Logger getLogger() {
		return LogManager.getLogger(getClass());
	}

	/**
	 * 
	 */

	@GET
	@Path(value = "/ping/")
	@Produces(value = JSON)
	public Response ping() {
		try {
			System.out.println(":: PING");

			return Response.ok("ok").build();
		} catch (Exception e) {
			this.getLogger().error(":: [" + this.getClass().getSimpleName() + "]", e.getMessage());

			System.out.println(":: [" + this.getClass().getSimpleName() + "]");

			return Response.serverError().build();
		} finally {

		}
	}

}