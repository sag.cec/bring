package pt.bring.bank_ws.control.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

@Component
@Provider
public class ProxyFilter implements ContainerRequestFilter, ContainerResponseFilter {

	/**
	 * 
	 */

	@Override
	public void filter(ContainerRequestContext crc) throws IOException {
		try {
			System.out.println(":: FILTER ContainerRequestContext");
		} catch (Exception e) {
			e.printStackTrace();

			crc.abortWith(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build());
		} finally {

		}
	}

	@Override
	public void filter(ContainerRequestContext crcRequest, ContainerResponseContext crcResponse) throws IOException {
		System.out.println(":: FILTER ContainerRequestContext ContainerResponseContext");

		crcResponse.getHeaders().add("Access-Control-Allow-Origin", "*");
		crcResponse.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		crcResponse.getHeaders().add("Access-Control-Allow-Headers",
				"origin,content-type,accept,authorization,X-Requested-With");
		crcResponse.getHeaders().add("Access-Control-Allow-Credentials", "true");
		crcResponse.getHeaders().add("Access-Control-Max-Age", "1209600");
	}

}