package pt.bring.bank_model.model.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Repository;

import pt.bring.bank_model.model.entity.AccountEntity;

/**
 * 
 * @author Gutemberg A Da Silva
 * 
 */

@Repository
public class AccountDaoImpl extends AbstractDaoImpl<AccountEntity> implements AccountDao<AccountEntity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * GERAL
	 */

	@Override
	public Class<AccountEntity> getEntityClass() {
		return AccountEntity.class;
	}

	/**
	 * ADMIN
	 */

	/**
	 * WS
	 */

	@Override
	public AccountEntity findBy(String value) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	@Override
	public BigDecimal amount(String type) {
		return null;
	}

	@Override
	public List<AccountEntity> search(Long id) {
		return null;
	}

}