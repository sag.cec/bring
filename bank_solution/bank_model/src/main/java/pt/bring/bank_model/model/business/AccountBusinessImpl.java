package pt.bring.bank_model.model.business;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import pt.bring.bank_model.model.dao.AccountDao;
import pt.bring.bank_model.model.entity.AccountEntity;

@Service("accountBusinessImpl")
public class AccountBusinessImpl extends AbstractBusinessImpl<AccountEntity> implements AccountBusiness {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Resource
	private AccountDao<AccountEntity> dao;

	@Override
	public AccountDao<AccountEntity> getDao() {
		return dao;
	}

	public void setDao(AccountDao<AccountEntity> dao) {
		this.dao = dao;
	}

	/**
	 * GERAL
	 */

	/**
	 * ADMIN
	 */

	/**
	 * WS
	 */

	@Override
	public AccountEntity findBy(Long id) throws Exception {
		return this.getDao().getEntity(id);
	}

	@Override
	public AccountEntity findBy(AccountEntity entity) throws Exception {
		return this.getDao().getEntity(entity);
	}

	@Override
	public BigDecimal amount(String type) {
		return this.getDao().amount(type);
	}

	@Override
	public List<AccountEntity> search(Long id) {
		return this.getDao().search(id);
	}

}