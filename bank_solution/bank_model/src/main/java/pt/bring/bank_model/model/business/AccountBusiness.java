package pt.bring.bank_model.model.business;

import java.math.BigDecimal;
import java.util.List;

import com.npti.npti_fw.model.business.AbstractBusiness;

import pt.bring.bank_model.model.entity.AccountEntity;

public interface AccountBusiness extends AbstractBusiness<AccountEntity> {

	/**
	 * ******************************************************************
	 * ***************************** ADMIN ******************************
	 * ******************************************************************
	 */

	/**
	 * ******************************************************************
	 * ****************************** WEB *******************************
	 * ******************************************************************
	 */

	/**
	 * ******************************************************************
	 * ******************************* WS *******************************
	 * ******************************************************************
	 */

	public BigDecimal amount(String type);

	public List<AccountEntity> search(Long id);

}