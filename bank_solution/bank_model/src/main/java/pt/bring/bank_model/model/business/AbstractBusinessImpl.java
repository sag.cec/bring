package pt.bring.bank_model.model.business;

import java.io.Serializable;
import java.util.List;

import com.npti.npti_fw.control.util.ObjectUtils;
import com.npti.npti_fw.control.util.StringUtils;
import com.npti.npti_fw.model.business.AbstractBusiness;
import com.npti.npti_fw.model.dao.AbstractDao;
import com.npti.npti_fw.model.entity.AbstractEntity;

public abstract class AbstractBusinessImpl<T extends AbstractEntity<? extends Serializable>>
		implements AbstractBusiness<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract AbstractDao<T> getDao();

	@Override
	public List<T> autocomplete(String suggest) throws Exception {
		try {
			List<T> list = this.getDao().autocomplete(suggest);

			return list;
		} catch (Exception e) {
			throw e;
		} finally {

		}
	}

	@Override
	public void delete(T t) throws Exception {
		try {
			this.getDao().update(t);
		} catch (Exception e) {
			throw e;
		} finally {

		}
	}

	@Override
	public void enabled(T t) throws Exception {
		try {
			this.getDao().update(t);
		} catch (Exception e) {
			throw e;
		} finally {

		}
	}

	@Override
	public List<T> list() throws Exception {
		try {
			List<T> list = this.getDao().list();

			return list;
		} catch (Exception e) {
			throw e;
		} finally {

		}
	}

	@Override
	public void saveOrUpdate(T t) throws Exception {
		try {
			// if (validate(entity)) {
			if (ObjectUtils.isBlank(t.getId())) {
				this.getDao().insert(t);
			} else {
				this.getDao().update(t);
			}
			// }
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	@Override
	public void saveOrUpdate(List<T> list) throws Exception {
		try {
			for (T t : list) {
				if (ObjectUtils.isBlank(t.getId())) {
					this.getDao().insert(t);
				} else {
					this.getDao().update(t);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	@Override
	public List<T> search(T t) throws Exception {
		try {
			List<T> list = null;

			if (ObjectUtils.isNotBlank(t) && StringUtils.isNotBlank(t.getDenominacao())) {
				list = this.getDao().search(t.getDenominacao());
			}

			return list;
		} catch (Exception e) {
			throw e;
		} finally {

		}
	}

}