package pt.bring.bank_model.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.npti.npti_fw.model.entity.AbstractEntity;

public class AccountEntity extends AbstractEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Long accountId; // this_account.id

	private Long counterpartyAccount; // other_account.number

	private String counterpartyName; // other_account.holder.name

	private String counterPartyLogoPath; // other_account.metadata.image_URL

	/**
	 * 
	 */

	private AccountEntity otherAccount;

	// @JsonBackReference
	private List<DetailEntity> detailList;

	/**
	 *
	 */

	public AccountEntity() {
		this.detailList = new ArrayList<DetailEntity>();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getCounterpartyAccount() {
		return counterpartyAccount;
	}

	public void setCounterpartyAccount(Long counterpartyAccount) {
		this.counterpartyAccount = counterpartyAccount;
	}

	public String getCounterpartyName() {
		return counterpartyName;
	}

	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}

	public String getCounterPartyLogoPath() {
		return counterPartyLogoPath;
	}

	public void setCounterPartyLogoPath(String counterPartyLogoPath) {
		this.counterPartyLogoPath = counterPartyLogoPath;
	}

	public AccountEntity getOtherAccount() {
		return otherAccount;
	}

	public void setOtherAccount(AccountEntity otherAccount) {
		this.otherAccount = otherAccount;
	}

	public List<DetailEntity> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<DetailEntity> detailList) {
		this.detailList = detailList;
	}

	/**
	 * 
	 */

	@Override
	public String getDenominacao() {
		return null;
	}

	@Override
	public void setDenominacao(String denominacao) {

	}

	@Override
	public Date getDataAlteracao() {
		return null;
	}

	@Override
	public void setDataAlteracao(Date d) {

	}

	@Override
	public Boolean getCancelado() {
		return null;
	}

	@Override
	public void setCancelado(Boolean cancelado) {

	}

	/**
	 *
	 */

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}