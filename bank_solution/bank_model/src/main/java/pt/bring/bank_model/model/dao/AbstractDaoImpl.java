package pt.bring.bank_model.model.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.npti.npti_fw.model.entity.AbstractEntity;

public abstract class AbstractDaoImpl<T extends AbstractEntity<? extends Serializable>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final Integer MAX_RESULT = 1;

	// manter sessao
	public void connection(T t) throws Exception {

	}

	// excluir
	public void delete(T t, Long id) throws Exception {

	}

	public void flushAndClear() throws Exception {

	}

	// inserir
	public void insert(T t) throws Exception {

	}

	// atualizar
	public void update(T t) throws Exception {
		t.setDataAlteracao(new Date());
	}

	/*****************************************
	 ****************** LOTE *****************
	 *****************************************/

	public void insert(List<T> list) throws Exception {
		for (T t : list) {
			this.insert(t);
		}
	}

	public void update(List<T> list) throws Exception {
		for (T t : list) {
			this.update(t);
		}
	}

	//

	public abstract Class<T> getEntityClass();

	/**
	 * MÉTODO
	 */

	public List<T> autocomplete(String suggest) throws Exception {
		try {
			List<T> list = new ArrayList<T>();

			return list;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public List<T> autocompleteAll(String suggest) throws Exception {
		try {
			List<T> list = new ArrayList<T>();

			return list;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public T findFirst() throws Exception {
		try {
			List<T> list = new ArrayList<T>();

			return list.stream().findFirst().orElse(null);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public T getEntity(Long id) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public T getEntity(T t) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public List<T> list() throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public List<T> listAll() throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public List<T> search(String value) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public List<T> searchAll(String value) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

}