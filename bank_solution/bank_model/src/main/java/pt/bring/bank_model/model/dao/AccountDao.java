package pt.bring.bank_model.model.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.npti.npti_fw.model.dao.AbstractDao;
import com.npti.npti_fw.model.entity.AbstractEntity;

public interface AccountDao<T extends AbstractEntity<? extends Serializable>> extends AbstractDao<T> {

	/**
	 * ******************************************************************
	 * ***************************** ADMIN ******************************
	 * ******************************************************************
	 */

	/**
	 * ******************************************************************
	 * ****************************** WEB *******************************
	 * ******************************************************************
	 */

	/**
	 * ******************************************************************
	 * ******************************* WS *******************************
	 * ******************************************************************
	 */

	public T findBy(String value) throws Exception;

	public BigDecimal amount(String type);

	public List<T> search(Long id);

}