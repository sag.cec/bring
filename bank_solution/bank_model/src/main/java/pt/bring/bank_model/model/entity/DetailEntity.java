package pt.bring.bank_model.model.entity;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.npti.npti_fw.model.entity.AbstractEntity;

public class DetailEntity extends AbstractEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Double instructedAmount; // details.value.amount

	private Double instructedCurrency; // details.value.currency

	private Double transactionAmount; // details.value.amount

	private Double transactionCurrency; // details.value.currency

	private String transactionType; // details.type

	private String description; // details.description

	/**
	 *
	 */

	// @JsonIgnoreProperties({ "accountId", "counterpartyAccount",
	// "counterpartyName", "counterPartyLogoPath",
	// "detailList" })
	private AccountEntity accountEntity;

	/**
	 *
	 */

	public DetailEntity() {

	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Double getInstructedAmount() {
		return instructedAmount;
	}

	public void setInstructedAmount(Double instructedAmount) {
		this.instructedAmount = instructedAmount;
	}

	public Double getInstructedCurrency() {
		return instructedCurrency;
	}

	public void setInstructedCurrency(Double instructedCurrency) {
		this.instructedCurrency = instructedCurrency;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Double getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(Double transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AccountEntity getAccountEntity() {
		return accountEntity;
	}

	public void setAccountEntity(AccountEntity accountEntity) {
		this.accountEntity = accountEntity;
	}

	/**
	 * 
	 */

	@Override
	public String getDenominacao() {
		return null;
	}

	@Override
	public void setDenominacao(String denominacao) {

	}

	@Override
	public Date getDataAlteracao() {
		return null;
	}

	@Override
	public void setDataAlteracao(Date d) {

	}

	@Override
	public Boolean getCancelado() {
		return null;
	}

	@Override
	public void setCancelado(Boolean cancelado) {

	}

	/**
	 *
	 */

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}