package com.npti.npti_fw.control.util;

import java.util.Arrays;

import com.npti.npti_fw.model.entity.AbstractEntity;

public class ObjectUtils extends org.apache.commons.lang3.ObjectUtils {

	public synchronized static boolean isBlank(Object obj) {
		boolean b = isNull(obj);

		if (b) {
			return b;
		}

		b = obj.equals("");

		return b;
	}

	public synchronized static boolean isNotBlankId(AbstractEntity<?> obj) {
		if (isNotNull(obj)) {
			if (obj.getId() != null) {
				if (Number.class.isAssignableFrom(obj.getId().getClass())) {
					Number number = (Number) obj.getId();

					return (number.longValue() > 0);
				}
			}
		}

		return false;
	}

	public synchronized static boolean isNotBlank(Object obj) {
		boolean b = isNotNull(obj);

		if (!b) {
			return b;
		}

		b = obj.equals("");

		return !b;
	}

	public synchronized static boolean isNotNull(Object obj) {
		return obj != null;
	}

	public synchronized static boolean isNotNull(Object... obj) {
		boolean b = isNotNull(obj);

		if (!b) {
			return b;
		}

		return !Arrays.asList(obj).contains(null);
	}

	public synchronized static boolean isNull(Object obj) {
		return obj == null;
	}

	public synchronized static boolean isNull(Object... obj) {
		boolean b = isNull(obj);

		if (b) {
			return b;
		}

		return Arrays.asList(obj).contains(null);
	}

	/**
	 * Teste
	 */

	public static void main(String[] args) {
		String a = null;

		System.out.println(isNotNull(a));

		String[] s = null;

		System.out.println(isNotNull(s));
	}

}