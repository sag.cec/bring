package com.npti.npti_fw.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public abstract class AbstractEntity<ID extends Serializable> implements Serializable {

	public abstract ID getId();

	public abstract void setId(ID id);

	public abstract String getDenominacao();

	public abstract void setDenominacao(String denominacao);

	public abstract Date getDataAlteracao();

	public abstract void setDataAlteracao(Date d);

	public abstract Boolean getCancelado();

	public abstract void setCancelado(Boolean cancelado);

	/*
	 * HASHCODE E EQUALS
	 */

	@Override
	public int hashCode() {
		return Objects.hash(this.getId(), this.getDenominacao());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (obj instanceof AbstractEntity) {
			AbstractEntity entity = (AbstractEntity) obj;

			return Objects.equals(this.getId(), entity.getId())
					&& Objects.equals(this.getDenominacao(), entity.getDenominacao());
		}

		return false;
	}

}