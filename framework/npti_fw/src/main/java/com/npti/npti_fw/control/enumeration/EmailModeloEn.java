package com.npti.npti_fw.control.enumeration;

public enum EmailModeloEn {

	TEST(0) {

		@Override
		public String toString() {
			String body = "<html>                                                                                          ";
			body += "         <head>                                                                                       ";
			body += "            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />                     ";
			body += "         </head>                                                                                      ";

			body += "         <body style='BACKGROUND: #F5F5F5; PADDING: 20px;'>                                           ";
			body += "            <div style='WIDTH: 100%; ALIGN: CENTER; DISPLAY: BLOCK;'>                                 ";
			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";

			body += "                  <a href='http://cat.cb.ce.gov.br'>                                                  ";
			body += "                     <img style='FLOAT: RIGHT;' src=\"cid:appLogo\" />                                ";
			body += "                  </a>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h2>                                                                                ";
			body += "                     Olá $NOME$, bem-vindo a Coordenadoria de Atividades Técnicas - CAT.              ";
			body += "                  </h2>                                                                               ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        Este e-mail tem como objetivo lhe guiar sobre o preenchimento de informações  ";
			body += "                        auxiliares.                                                                   ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        $CONTEUDO$                                                                    ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h3>                                                                                ";
			body += "                     Para a utilização do sistema, $NOME$, você deverá utilizar como login seu CPF e  ";
			body += "                     a senha abaixo                                                                   ";

			body += "                     <br />                                                                           ";

			body += "                     Senha .....: <font color='#FF0000'><b>$SENHA$</b></font>                         ";
			body += "                  <h3>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='BACKGROUND-COLOR: #8EDEAD; PADDING: 5px; MARGIN: AUTO; TEXT-ALIGN: CENTER; ";
			body += "                  BORDER-RADIUS: 10px; DISPLAY: BLOCK;'>                                              ";
			body += "                  <h1>                                                                                ";
			body += "                     <a href='$LINK$' style='COLOR: #FFFFFF;'>                                        ";
			body += "                        Clique aqui para acessar o sistema                                            ";
			body += "                     </a>                                                                             ";
			body += "                  </h1>                                                                               ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='TEXT-ALIGN: CENTER; FONT-SIZE: 12px; FONT-WEIGHT: BOLD; DISPLAY: BLOCK;'>  ";
			body += "                  Corpo de Bombeiros Militar do Estado do Ceará                                       ";

			body += "                  <br />                                                                              ";

			body += "                  Coordenadoria de Atividades Técnicas - CAT                                          ";

			body += "                  <br />                                                                              ";

			body += "                  Rua Liberato Barroso, 1400 - Jacarecanga - Fortaleza / CE                           ";

			body += "                  <br />                                                                              ";

			body += "                  CEP: 60030-161 - Fone: (85) 3101-2394                                               ";
			body += "               </div>                                                                                 ";
			body += "            </div>                                                                                    ";
			body += "         </body>                                                                                      ";
			body += "      </html>                                                                                         ";

			return body;
		}

	},

	PADRAO_INFO_SEM_ANEXO(1) {

		@Override
		public String toString() {
			String body = "<html>                                                                                          ";
			body += "         <head>                                                                                       ";
			body += "            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />                     ";
			body += "         </head>                                                                                      ";

			body += "         <body style='BACKGROUND: #F5F5F5; PADDING: 20px;'>                                           ";
			body += "            <div style='WIDTH: 100%; ALIGN: CENTER; DISPLAY: BLOCK;'>                                 ";
			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";

			body += "                  <a href='http://cat.cb.ce.gov.br'>                                                  ";
			body += "                     <img style='FLOAT: RIGHT;' src=\"cid:appLogo\" />                                ";
			body += "                  </a>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h2>                                                                                ";
			body += "                     Olá $NOME$, bem-vindo a Coordenadoria de Atividades Técnicas - CAT.              ";
			body += "                  </h2>                                                                               ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        Este e-mail tem como objetivo lhe guiar sobre o preenchimento de informações  ";
			body += "                        auxiliares.                                                                   ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        <font size='14'>$CONTEUDO$</font>                                             ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h3>                                                                                ";
			body += "                     Para a utilização do sistema, $NOME$, você deverá utilizar como login seu CPF e  ";
			body += "                     a senha abaixo                                                                   ";

			body += "                     <br />                                                                           ";

			body += "                     Senha .....: <font color='#FF0000'><b>$SENHA$</b></font>                         ";
			body += "                  <h3>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='BACKGROUND-COLOR: #8EDEAD; PADDING: 5px; MARGIN: AUTO; TEXT-ALIGN: CENTER; ";
			body += "                  BORDER-RADIUS: 10px; DISPLAY: BLOCK;'>                                              ";
			body += "                  <h1>                                                                                ";
			body += "                     <a href='$LINK$' style='COLOR: #FFFFFF;'>                                        ";
			body += "                        Clique aqui para acessar o sistema                                            ";
			body += "                     </a>                                                                             ";
			body += "                  </h1>                                                                               ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='TEXT-ALIGN: CENTER; FONT-SIZE: 12px; FONT-WEIGHT: BOLD; DISPLAY: BLOCK;'>  ";
			body += "                  Corpo de Bombeiros Militar do Estado do Ceará                                       ";

			body += "                  <br />                                                                              ";

			body += "                  Coordenadoria de Atividades Técnicas - CAT                                          ";

			body += "                  <br />                                                                              ";

			body += "                  Rua Liberato Barroso, 1400 - Jacarecanga - Fortaleza / CE                           ";

			body += "                  <br />                                                                              ";

			body += "                  CEP: 60030-161 - Fone: (85) 3101-2394                                               ";
			body += "               </div>                                                                                 ";
			body += "            </div>                                                                                    ";
			body += "         </body>                                                                                      ";
			body += "      </html>                                                                                         ";

			return body;
		}

	},

	RECUPERACAO_SENHA(2) {

		@Override
		public String toString() {
			String body = "<html>                                                                                          ";
			body += "         <head>                                                                                       ";
			body += "            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />                     ";
			body += "         </head>                                                                                      ";

			body += "         <body style='BACKGROUND: #F5F5F5; PADDING: 20px;'>                                           ";
			body += "            <div style='WIDTH: 100%; ALIGN: CENTER; DISPLAY: BLOCK;'>                                 ";
			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";

			body += "                  <a href='http://cat.cb.ce.gov.br'>                                                  ";
			body += "                     <img style='FLOAT: RIGHT;' src=\"cid:appLogo\" />                                ";
			body += "                  </a>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h2>                                                                                ";
			body += "                     Olá $NOME$, bem-vindo a Coordenadoria de Atividades Técnicas - CAT.              ";
			body += "                  </h2>                                                                               ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        Este e-mail tem como objetivo lhe informar a recuperação de senha.            ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        $CONTEUDO$                                                                    ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h3>                                                                                ";
			body += "                     Para a utilização do sistema, $NOME$, você deverá utilizar como login seu CPF e  ";
			body += "                     a nova senha abaixo                                                              ";

			body += "                     <br />                                                                           ";

			body += "                     Senha .....: <font color='#FF0000'><b>$SENHA$</b></font>                         ";
			body += "                  <h3>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='BACKGROUND-COLOR: #8EDEAD; PADDING: 5px; MARGIN: AUTO; TEXT-ALIGN: CENTER; ";
			body += "                  BORDER-RADIUS: 10px; DISPLAY: BLOCK;'>                                              ";
			body += "                  <h1>                                                                                ";
			body += "                     <a href='$LINK$' style='COLOR: #FFFFFF;'>                                        ";
			body += "                        Clique aqui para acessar o sistema                                            ";
			body += "                     </a>                                                                             ";
			body += "                  </h1>                                                                               ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='TEXT-ALIGN: CENTER; FONT-SIZE: 12px; FONT-WEIGHT: BOLD; DISPLAY: BLOCK;'>  ";
			body += "                  Corpo de Bombeiros Militar do Estado do Ceará                                       ";

			body += "                  <br />                                                                              ";

			body += "                  Coordenadoria de Atividades Técnicas - CAT                                          ";

			body += "                  <br />                                                                              ";

			body += "                  Rua Liberato Barroso, 1400 - Jacarecanga - Fortaleza / CE                           ";

			body += "                  <br />                                                                              ";

			body += "                  CEP: 60030-161 - Fone: (85) 3101-2394                                               ";
			body += "               </div>                                                                                 ";
			body += "            </div>                                                                                    ";
			body += "         </body>                                                                                      ";
			body += "      </html>                                                                                         ";

			return body;
		}

	},

	PADRAO_INDEFIRIDO_SEM_ANEXO(3) {

		@Override
		public String toString() {
			String body = "<html>                                                                                          ";
			body += "         <head>                                                                                       ";
			body += "            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />                     ";
			body += "         </head>                                                                                      ";

			body += "         <body style='BACKGROUND: #F5F5F5; PADDING: 20px;'>                                           ";
			body += "            <div style='WIDTH: 100%; ALIGN: CENTER; DISPLAY: BLOCK;'>                                 ";
			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";

			body += "                  <a href='http://cat.cb.ce.gov.br'>                                                  ";
			body += "                     <img style='FLOAT: RIGHT;' src=\"cid:appLogo\" />                                ";
			body += "                  </a>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='WIDTH: 100%; DISPLAY: BLOCK;'>                                             ";
			body += "                  <h2>                                                                                ";
			body += "                     Bem-vindo a Coordenadoria de Atividades Técnicas - CAT.                          ";
			body += "                  </h2>                                                                               ";

			body += "                  <br />                                                                              ";

			body += "                  <p>                                                                                 ";
			body += "                     <span style='FONT-SIZE: 14px;'>                                                  ";
			body += "                        <font size='14'>$CONTEUDO$</font>                                             ";
			body += "                     </span>                                                                          ";
			body += "                  </p>                                                                                ";
			body += "               </div>                                                                                 ";

			body += "               <br /> <br />                                                                          ";

			body += "               <div style='TEXT-ALIGN: CENTER; FONT-SIZE: 12px; FONT-WEIGHT: BOLD; DISPLAY: BLOCK;'>  ";
			body += "                  Corpo de Bombeiros Militar do Estado do Ceará                                       ";

			body += "                  <br />                                                                              ";

			body += "                  Coordenadoria de Atividades Técnicas - CAT                                          ";

			body += "                  <br />                                                                              ";

			body += "                  Rua Liberato Barroso, 1400 - Jacarecanga - Fortaleza / CE                           ";

			body += "                  <br />                                                                              ";

			body += "                  CEP: 60030-161 - Fone: (85) 3101-2394                                               ";
			body += "               </div>                                                                                 ";
			body += "            </div>                                                                                    ";
			body += "         </body>                                                                                      ";
			body += "      </html>                                                                                         ";

			return body;
		}

	};

	private Integer key;

	private EmailModeloEn(Integer key) {
		this.key = key;
	}

	public Integer getKey() {
		return key;
	}

}