package com.npti.npti_fw.control.util;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

	public synchronized static String removeSpecialCharacters(String string) {
		return string.replaceAll("\\W", "").trim();
	}

	public synchronized static String onlyDigit(String string) {
		if (string.replaceAll("\\D", "").trim().equals("")) {
			return "0";
		}

		return string.replaceAll("\\D", "").trim();
	}

	public synchronized static String onlyLetter(String string) {
		return string.replaceAll("\\d", "").trim();
	}

	public synchronized static String removeAccentuation(String str) {
		return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

	/**
	 * Teste
	 */

	public static void main1(String[] args) {
		String pattern = "(\\d)(\\s+)([\\.,])";
		System.out.println("abahg$$555".replaceAll(pattern, "$1$3"));
		
		/*
		 * System.out.println(removeSpecialCharacters(
		 * "...KKK..;;;[[]]][[{{}}}lll++-eeeee-"));
		 * 
		 * System.out.println(onlyDigit("teste1234jkfjsfdksa457"));
		 * 
		 * System.out.println(onlyLetter("teste1234jkfjsfdksa457"));
		 */

		System.out.println(removeAccentuation("maçã tônica"));

		String[] o = "1896f809-e995-451a-9a79-7e08d81dc763.3.10.BMP".split("[.]");

		System.out.println(o[1] + " " + o[2]);
	}
	
	public static final String EXAMPLE_TEST = "This is my small example "
            + "string which I'm going to " + "use for pattern matching.";

    public static void main(String[] args) {
    	Pattern pattern = Pattern.compile("\\Bjava\\B");
    	Matcher matcher = pattern.matcher("10javal");

    	System.out.println("matcher.matches() = " + matcher.matches());
    	
    	
    	
    	/*System.out.println("9()(//86&&&".matches("\\W.+"));
    	
    	
    	System.out.println(EXAMPLE_TEST.matches("\\w.*"));
    	System.out.println(EXAMPLE_TEST.matches("\\W.*"));
    	
        System.out.println(EXAMPLE_TEST.matches("\\w.*"));
        
        String[] splitString = (EXAMPLE_TEST.split("\\s+"));
        System.out.println(splitString.length);// should be 14
        for (String string : splitString) {
            System.out.println(string);
        }*/
        // replace all whitespace with tabs
        //System.out.println(EXAMPLE_TEST.replaceAll("\\s+", "\t"));
    }

}