package com.npti.npti_fw.model.dao;

import java.io.Serializable;
import java.util.List;

import com.npti.npti_fw.model.entity.AbstractEntity;

/**
 *
 * @author Gutemberg A Da Silva
 * @param <T>
 *
 */
public interface AbstractDao<T extends AbstractEntity<? extends Serializable>> extends Serializable {

	public static final int QTD_MAX_REGISTROS = 150;

	// manter sessao
	public void connection(T entity) throws Exception;

	// excluir
	public void delete(T entity, Long id) throws Exception;

	public void flushAndClear() throws Exception;

	// inserir
	public void insert(T entity) throws Exception;

	// atualizar
	public void update(T entity) throws Exception;

	/*****************************************
	 ****************** LOTE *****************
	 *****************************************/

	// inserir
	public void insert(List<T> list) throws Exception;

	// atualizar
	public void update(List<T> list) throws Exception;

	/**
	 * 
	 */

	// public Class<T> getEntityClass();

	public List<T> autocomplete(String suggest) throws Exception;

	public T getEntity(Long id) throws Exception;

	public T getEntity(T entity) throws Exception;

	public List<T> list() throws Exception;

	public List<T> search(String value) throws Exception;

}