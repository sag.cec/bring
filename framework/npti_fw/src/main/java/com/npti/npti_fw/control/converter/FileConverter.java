package com.npti.npti_fw.control.converter;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;

import com.npti.npti_fw.control.util.NumberUtils;

public class FileConverter {

	public static byte[] toByteArray(InputStream is) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(is, out);
		IOUtils.closeQuietly(is);
		IOUtils.closeQuietly(out);
		byte[] bytes = out.toByteArray();

		return bytes;
	}

	public static void toImageFile(String dir, String title, byte[] bytes) throws Exception {
		FileOutputStream output = new FileOutputStream(new File(dir, title));
		IOUtils.write(bytes, output);
	}

	public static String toImageDimesion(String path) throws Exception {
		BufferedImage bi = ImageIO.read(new File(path));

		int width = bi.getWidth();
		int height = bi.getHeight();

		return "WIDTH: " + width + "px; HEIGHT: " + height + "px;";
	}

	public static Integer[] imageDimesion(InputStream is) throws Exception {
		BufferedImage bi = ImageIO.read(is);

		int width = bi.getWidth();
		int height = bi.getHeight();

		return new Integer[] { height, width };
	}

	public static String toImageDimesion(Integer height, Integer width) throws Exception {
		if (NumberUtils.isBlank(height)) {
			height = new Integer(0);
		}

		if (NumberUtils.isBlank(width)) {
			width = new Integer(0);
		}

		return "HEIGHT: " + height + "px; WIDTH: " + width + "px;";
	}

}