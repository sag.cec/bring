package com.npti.npti_fw.control.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.UUID;

public class UserUtils {

	public synchronized static String createPassword() {
		UUID uuid = UUID.randomUUID();
		String myRandom = uuid.toString();

		return myRandom.substring(0, 6);
	}

	public synchronized static boolean confirmPassword(String password, String repassword) {
		if (password.equals(repassword)) {
			return true;
		}

		return false;
	}

	public synchronized static String createMd5(String s) throws Exception {
		// String s="This is a test";
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.update(s.getBytes(), 0, s.length());
		// System.out.println("MD5: "+new
		// BigInteger(1,m.digest()).toString(16));

		return new BigInteger(1, m.digest()).toString(16);
	}

	/**
	 * 
	 */

	public static void main(String args[]) throws Exception {
		String s = "Texto de Exemplo";

		System.out.println(createMd5(s));
	}

}