package com.npti.npti_fw.control.enumeration;

public enum TipoEnderecoEn {

	R(1) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "RESIDENCIAL";
		}

	},

	C(2) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "COMERCIAL";
		}

	};

	private Integer key;

	private TipoEnderecoEn(Integer key) {
		this.key = key;
	}

	public Integer getKey() {
		return key;
	}

}