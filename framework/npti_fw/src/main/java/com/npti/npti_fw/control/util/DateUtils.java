package com.npti.npti_fw.control.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

	public synchronized static boolean isBlank(Date date) {
		boolean b = isNull(date);

		if (b) {
			return b;
		}

		b = date.equals("");

		return b;
	}

	public synchronized static boolean isNotBlank(Date date) {
		boolean b = isNotNull(date);

		if (!b) {
			return b;
		}

		b = date.equals("");

		return !b;
	}

	public synchronized static boolean isNotNull(Date date) {
		return date != null;
	}

	public synchronized static boolean isNull(Date date) {
		return date == null;
	}

	public synchronized static String parseString(GregorianCalendar dateSync) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = sdf.format(dateSync.getTime());

		return s;
	}

	public synchronized static Long diffBetweenNowAndAnyDate(Long timeInMillis) {
		long diff = new Date().getTime() - timeInMillis;

		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public synchronized static String toString(long date, String mask) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(date);

		SimpleDateFormat sdf;

		if (StringUtils.isNotBlank(mask)) {
			sdf = new SimpleDateFormat(mask);
		} else {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}

		String s = sdf.format(gc.getTime());

		return s;
	}

	public synchronized static Long toLong(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);

		return gc.getTimeInMillis();
	}

	public synchronized static Long toLong(String s) {
		String[] array = s.split("/");

		int day = Integer.parseInt(array[0]);
		int month = Integer.parseInt(array[1]);
		int year = Integer.parseInt(array[2]);

		Calendar c = Calendar.getInstance();
		c.set(year, month, day);

		return c.getTimeInMillis();
	}

	/**
	 * 
	 */

	public static void main(String[] args) {
		GregorianCalendar c = new GregorianCalendar(2000, 8, 19, 11, 52, 58);

		System.out.println(c.getTime().getTime());

		c.setTimeInMillis(new Long("969375178000"));

		System.out.println(c.getTime());

		// "2000-09-19 11:52:58.056"

		System.out.println(new Date(2000, 8, 19, 11, 52, 58).getTime());
	}

}