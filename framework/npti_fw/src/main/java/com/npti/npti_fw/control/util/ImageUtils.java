package com.npti.npti_fw.control.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;

import javax.imageio.ImageIO;

public class ImageUtils {

	public synchronized static void base64ToImage(String imageBase64, String pathDestination, String filename)
			throws Exception {
		// String base64Image = base64.split(",")[1];
		String[] s = imageBase64.split(",");
		s = s[0].split(";");
		String extensionfile = s[0].split("/")[1];

		base64ToImage(imageBase64, pathDestination, filename, extensionfile);
	}

	public synchronized static void base64ToImage(String imageBase64, String pathDestination, String filename,
			String extensionfile) throws Exception {
		// String base64Image = base64.split(",")[1];
		byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(imageBase64.split(",")[1]);

		BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));

		File outputfile = new File(pathDestination + File.separator + filename + "." + extensionfile);
		ImageIO.write(img, extensionfile, outputfile);
	}

	public synchronized static void base64ToImage(byte[] imageBytes, String pathDestination, String filename,
			String extensionfile) throws Exception {
		BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));

		File outputfile = new File(pathDestination + File.separator + filename + "." + extensionfile);
		ImageIO.write(img, extensionfile, outputfile);
	}

	/**
	 * ******************************************************************************
	 */

	public static void main(String[] args) {
		try {
			base64ToImage(
					"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAAAuklEQVR4AdXTvyqFcQDG8Y/CSTalJCaryWJiUgZ6J6vNYjHJIKuVO9BrcAESd0JKGSkD+RuOetzB7/yWM5zP/mzP12AYFvHXt8G6SxFxZl5PxyK+PfgRX5YVLYnY1cG4VtwqOhA3ACZFzCnYEa9mVZvyIp4dWjCkyqJ7EfHoyIwKozZdeBfxoVFpRONafJpWbcKb2FZwIk4BcCf2FayJri0djNkTsVJzjV9PuiJaPa06FxFXNvrcw6D4B/OlT9+wHSm1AAAAAElFTkSuQmCC",
					"c:\\Temp\\", "ant54545454");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}