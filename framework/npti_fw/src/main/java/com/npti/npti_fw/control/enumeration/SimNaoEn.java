package com.npti.npti_fw.control.enumeration;

public enum SimNaoEn {

	NAO(0) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "NÃO";
		}

	},

	SIM(1) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "SIM";
		}

	};

	private Integer key;

	private SimNaoEn(Integer key) {
		this.key = key;
	}

	public Integer getKey() {
		return key;
	}

}