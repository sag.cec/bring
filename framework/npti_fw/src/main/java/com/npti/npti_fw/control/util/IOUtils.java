package com.npti.npti_fw.control.util;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by gutemberg.silva on 23/01/2017.
 */

public class IOUtils extends org.apache.commons.io.IOUtils {

	public static void copyAndClose(InputStream is, OutputStream os) {
		try {
			org.apache.commons.io.IOUtils.copy(is, os);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			org.apache.commons.io.IOUtils.closeQuietly(is);
			org.apache.commons.io.IOUtils.closeQuietly(os);
		}
	}

}