package com.npti.npti_fw.model.business;

import java.io.Serializable;

import com.npti.npti_fw.model.entity.AbstractEntity;

/**
 *
 * @author ednardorubens
 * @param <T>
 */
public abstract class AbstractBusinessImpl<T extends AbstractEntity<? extends Serializable>>
		implements AbstractBusiness<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * @Override public T buscarPorId(T entidade) { T aux = null;
	 * 
	 * try { aux = getDao().buscarPorId(entidade); } catch (Exception ex) {
	 * ex.printStackTrace(); }
	 * 
	 * return aux; }
	 */

}