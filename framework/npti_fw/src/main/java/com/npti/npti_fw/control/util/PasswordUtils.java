package com.npti.npti_fw.control.util;

import java.util.UUID;

public class PasswordUtils {

	public synchronized static String random() {
		UUID uuid = UUID.randomUUID();

		return StringUtils.removeSpecialCharacters(uuid.toString().substring(0, 10));
	}

	public synchronized static String random(int size) {
		UUID uuid = UUID.randomUUID();

		return StringUtils.removeSpecialCharacters(uuid.toString().substring(0, size));
	}

	public synchronized static String randomWithSpecialCharacters() {
		UUID uuid = UUID.randomUUID();

		return uuid.toString().substring(0, 10);
	}

	public synchronized static String randomWithSpecialCharacters(int size) {
		UUID uuid = UUID.randomUUID();

		return StringUtils.removeSpecialCharacters(uuid.toString().substring(0, size));
	}

}