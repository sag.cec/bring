package com.npti.npti_fw.control.enumeration;

public enum SexoEn {

	M(1) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "MASCULINO";
		}

	},

	F(2) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "FEMININO";
		}

	},

	I(3) {

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "INDIFERENTE";
		}

	};

	private Integer key;

	private SexoEn(Integer key) {
		this.key = key;
	}

	public Integer getKey() {
		return key;
	}

}