package com.npti.npti_fw.control.util;

import java.util.Arrays;
import java.util.List;

public class ListUtils {

	public synchronized static boolean isEmpty(List list) {
		return list.isEmpty();
	}

	public synchronized static boolean isNotEmpty(List list) {
		return !list.isEmpty();
	}

	public synchronized static boolean isNull(List list) {
		return list == null;
	}

	public synchronized static boolean isNotNull(List list) {
		return list != null;
	}

	public synchronized static boolean isNotNullAndNotEmpty(List list) {
		boolean b = isNotNull(list);

		if (!b) {
			return b;
		}

		b = isNotEmpty(list);

		return b;
	}

	public synchronized static boolean isNotNullAndNotEmpty(Object[] array) {
		return isNotNullAndNotEmpty(Arrays.asList(array));
	}

	public synchronized static boolean isNullOrEmpty(List list) {
		boolean b = isNull(list);

		if (b) {
			return b;
		}

		b = isEmpty(list);

		return b;
	}

	public synchronized static boolean isNullOrEmpty(Object[] array) {
		return isNullOrEmpty(Arrays.asList(array));
	}

	/**
	*
	*/

	public synchronized static Object last(Object[] array) {
		if (isNullOrEmpty(array)) {
			return null;
		}

		List list = Arrays.asList(array);

		return list.get(list.size() - 1);
	}

}