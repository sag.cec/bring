package com.npti.npti_fw.control.util;

import java.math.BigDecimal;

public class NumberUtils extends org.apache.commons.lang3.math.NumberUtils {

	public synchronized static boolean isBlank(Integer number) {
		boolean b = isNull(number);

		if (b) {
			return b;
		}

		b = number.equals("");

		return b;
	}

	public synchronized static boolean isBlank(BigDecimal number) {
		boolean b = isNull(number);

		if (b) {
			return b;
		}

		b = number.equals("");

		return b;
	}

	public synchronized static boolean isBlank(Double number) {
		boolean b = isNull(number);

		if (b) {
			return b;
		}

		b = number.equals("");

		return b;
	}

	public synchronized static boolean isBlank(Long number) {
		boolean b = isNull(number);

		if (b) {
			return b;
		}

		b = number.equals("");

		return b;
	}

	/**
	 * 
	 */

	public synchronized static boolean isNotBlank(Integer number) {
		boolean b = isNotNull(number);

		if (!b) {
			return b;
		}

		b = number.equals("");

		return !b;
	}

	public synchronized static boolean isNotBlank(BigDecimal number) {
		boolean b = isNotNull(number);

		if (!b) {
			return b;
		}

		b = number.equals("");

		return !b;
	}

	public synchronized static boolean isNotBlank(Double number) {
		boolean b = isNotNull(number);

		if (!b) {
			return b;
		}

		b = number.equals("");

		return !b;
	}

	public synchronized static boolean isNotBlank(Long number) {
		boolean b = isNotNull(number);

		if (!b) {
			return b;
		}

		b = number.equals("");

		return !b;
	}

	/**
	 * 
	 */

	public synchronized static boolean isNotNull(Integer number) {
		return !isNull(number);
	}

	public synchronized static boolean isNotNull(BigDecimal number) {
		return !isNull(number);
	}

	public synchronized static boolean isNotNull(Double number) {
		return !isNull(number);
	}

	public synchronized static boolean isNotNull(Long number) {
		return !isNull(number);
	}

	/**
	 * 
	 */

	public synchronized static boolean isNull(Integer number) {
		return number == null;
	}

	public synchronized static boolean isNull(BigDecimal number) {
		return number == null;
	}

	public synchronized static boolean isNull(Double number) {
		return number == null;
	}

	public synchronized static boolean isNull(Long number) {
		return number == null;
	}

	/**
	 * 
	 */

	public synchronized static BigDecimal createBigDecimal(Object obj) {
		return (BigDecimal) obj;
	}

	public synchronized static int toInt(Object obj) {
		return (Integer) obj;
	}

	/**
	 * 
	 */

	public static void main(String[] args) {

	}

}