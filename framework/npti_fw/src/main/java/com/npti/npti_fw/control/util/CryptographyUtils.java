package com.npti.npti_fw.control.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class CryptographyUtils {

	public synchronized static String md5(String s) throws Exception {
		// String s="Texto de Exemplo";
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(s.getBytes("utf8"));

		return new BigInteger(1, md.digest()).toString(16);
	}

	public synchronized static String sha1(String s) throws Exception {
		// String s="Texto de Exemplo";
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(s.getBytes("utf8"));

		return new BigInteger(1, md.digest()).toString(16);
	}

	public static void main(String[] args) throws Exception {
		System.out.println(sha1("sspds123"));
	}

}