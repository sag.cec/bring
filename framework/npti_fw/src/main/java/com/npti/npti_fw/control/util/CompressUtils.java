package com.npti.npti_fw.control.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;

public class CompressUtils {

	public synchronized static String compress(String string) throws Exception {
		if (StringUtils.isBlank(string)) {
			return string;
		}

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		GZIPOutputStream gzip = new GZIPOutputStream(out);
		gzip.write(string.getBytes("UTF-8"));
		gzip.close();

		return out.toString();
	}

	public static void main(String[] args) throws Exception {
		String string = "admin";
		System.out.println("after compress:");
		System.out.println(compress(string));
	}

}