package com.npti.npti_fw.control.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class XmlUtils {

	public synchronized static Object getNode(String xml, String expression, QName qn) throws Exception {
		try {
			InputSource is = new InputSource(new StringReader(xml));
			XPath xp = XPathFactory.newInstance().newXPath();

			xp.setNamespaceContext(new NamespaceContext() {

				@Override
				public String getNamespaceURI(String prefix) {
					return "http://service.sspds.ce.gov.br/xsd";
				}

				@Override
				public String getPrefix(String namespaceURI) {
					return "ns";
				}

				@Override
				public Iterator getPrefixes(String namespaceURI) {
					Set s = new HashSet();
					s.add("ns");

					return s.iterator();
				}

			});

			return xp.evaluate(expression, is, qn);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public synchronized static Object getNode(String xml, String expression, QName qn, final String nsUri,
			final String prefix) throws Exception {
		try {
			InputSource is = new InputSource(new StringReader(xml));
			XPath xp = XPathFactory.newInstance().newXPath();

			xp.setNamespaceContext(new NamespaceContext() {

				@Override
				public String getNamespaceURI(String prefix) {
					return nsUri;
				}

				@Override
				public String getPrefix(String namespaceURI) {
					return prefix;
				}

				@Override
				public Iterator getPrefixes(String namespaceURI) {
					Set s = new HashSet();
					s.add(prefix);

					return s.iterator();
				}

			});

			return xp.evaluate(expression, is, qn);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public synchronized static Object getObject(Class<?> clazz, byte[] b) throws Exception {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			InputStream is = new ByteArrayInputStream(b);
			Reader r = new InputStreamReader(is, "UTF-8");
			InputSource iso = new InputSource(r);
			iso.setEncoding("UTF-8");

			Document doc = dbf.newDocumentBuilder().parse(iso);

			JAXBContext jc = JAXBContext.newInstance(clazz);

			/**
			 * 5 the only difference with the marshaling operation is here 6
			 */

			return jc.createUnmarshaller().unmarshal(doc);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public synchronized static Object getObject(Class<?> clazz, String xml) throws Exception {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			InputStream is = new ByteArrayInputStream(xml.getBytes());
			Reader r = new InputStreamReader(is, "UTF-8");
			InputSource iso = new InputSource(r);
			iso.setEncoding("UTF-8");

			Document doc = dbf.newDocumentBuilder().parse(iso);

			JAXBContext jc = JAXBContext.newInstance(clazz);

			/**
			 * 5 the only difference with the marshaling operation is here 6
			 */

			return jc.createUnmarshaller().unmarshal(doc);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public synchronized static Object getObject(Class<?> clazz, String xml, String encodedCode) throws Exception {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			InputStream is = new ByteArrayInputStream(xml.getBytes());
			Reader r = new InputStreamReader(is, encodedCode);
			InputSource iso = new InputSource(r);
			iso.setEncoding(encodedCode);

			Document doc = dbf.newDocumentBuilder().parse(iso);

			JAXBContext jc = JAXBContext.newInstance(clazz);

			/**
			 * 5 the only difference with the marshaling operation is here 6
			 */

			return jc.createUnmarshaller().unmarshal(doc);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

	public synchronized static String getXml(byte[] b) throws Exception {
		try {
			Source s = new SAXSource(new InputSource(new ByteArrayInputStream(b)));
			StreamResult sr = new StreamResult(new ByteArrayOutputStream());

			Transformer t = SAXTransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			t.transform(s, sr);

			return new String(((ByteArrayOutputStream) sr.getOutputStream()).toByteArray());
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		} finally {

		}
	}

}