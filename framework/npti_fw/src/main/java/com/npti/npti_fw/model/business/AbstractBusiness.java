package com.npti.npti_fw.model.business;

import java.io.Serializable;
import java.util.List;

import com.npti.npti_fw.model.dao.AbstractDao;
import com.npti.npti_fw.model.entity.AbstractEntity;

/**
 * 
 * @author Gutemberg A Da Silva
 * 
 */

public interface AbstractBusiness<T extends AbstractEntity<? extends Serializable>> extends Serializable {

	public AbstractDao<T> getDao();

	public List<T> autocomplete(String suggest) throws Exception;

	public void delete(T entity) throws Exception;

	public void enabled(T entity) throws Exception;

	public T findBy(Long id) throws Exception;

	public T findBy(T entity) throws Exception;

	public List<T> list() throws Exception;

	public void saveOrUpdate(List<T> list) throws Exception;

	public void saveOrUpdate(T entity) throws Exception;

	public List<T> search(T entity) throws Exception;

	/**
	 * 
	 */

}