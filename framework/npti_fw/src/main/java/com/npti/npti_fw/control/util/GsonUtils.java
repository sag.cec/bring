package com.npti.npti_fw.control.util;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GsonUtils extends org.apache.commons.lang3.ClassUtils {

	public synchronized static Gson newInstance() {
		GsonBuilder builder = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

			@Override
			public Date deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
					throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}

		}).registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {

			@Override
			public byte[] deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
					throws JsonParseException {
				try {
					List list = new ArrayList();

					JsonArray ja = json.getAsJsonArray();

					for (JsonElement je : ja) {
						list.add(je.getAsByte());
					}

					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					ObjectOutputStream oos = new ObjectOutputStream(bos);
					oos.writeObject(list);
					byte[] bytes = bos.toByteArray();

					return bytes;
				} catch (Exception e) {
					e.printStackTrace();
				} finally {

				}

				return null;
			}

		}).registerTypeAdapter(Object.class, new JsonDeserializer<Object>() {

			@Override
			public List<Object> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
				List<Object> list = new ArrayList<Object>();

				if (json.isJsonArray()) {
					for (JsonElement e : json.getAsJsonArray()) {
						list.add((Object) ctx.deserialize(e, Object.class));
					}
				} else if (json.isJsonObject()) {
					list.add((Object) ctx.deserialize(json, Object.class));
				} else {
					throw new RuntimeException("Unexpected JSON type: " + json.getClass());
				}

				return list;
			}

		}).registerTypeAdapter(byte[].class, new JsonSerializer<byte[]>() {

			@Override
			public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
				return new JsonPrimitive(Base64.encodeBase64String(src));
			}

		});

		/*
		 * .registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {
		 * 
		 * @Override public byte[] deserialize(JsonElement json, Type arg1,
		 * JsonDeserializationContext arg2) throws JsonParseException { return
		 * json.getAsString().getBytes(); }
		 * 
		 * });
		 */

		return builder.create();
	}

	public synchronized static GsonBuilder newBuilder() {
		GsonBuilder builder = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

			@Override
			public Date deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
					throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}

		}).registerTypeAdapter(List.class, new JsonDeserializer<List>() {

			@Override
			public List deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
					throws JsonParseException {
				List list = new ArrayList();

				JsonArray ja = json.getAsJsonArray();

				for (JsonElement je : ja) {
					list.add(je.getAsJsonObject());
				}

				return list;
			}

		}).registerTypeAdapter(Object[].class, new JsonDeserializer<Object[]>() {

			@Override
			public Object[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {
				List list = new ArrayList();

				JsonArray ja = json.getAsJsonArray();

				for (JsonElement je : ja) {
					list.add(je.getAsJsonArray());
				}

				return list.toArray();
			}

		}).registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {

			@Override
			public byte[] deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
					throws JsonParseException {
				/*
				 * try { List list = new ArrayList();
				 * 
				 * JsonArray ja = json.getAsJsonArray();
				 * 
				 * for (JsonElement je : ja) { list.add(je.getAsByte()); }
				 * 
				 * ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutputStream
				 * oos = new ObjectOutputStream(bos); oos.writeObject(list);
				 * 
				 * byte[] bytes = bos.toByteArray();
				 * 
				 * return bytes; } catch (Exception e) { e.printStackTrace(); } finally {
				 * 
				 * }
				 * 
				 * return null;
				 */

				return Base64.decodeBase64(json.getAsString());
			}

		}).registerTypeAdapter(byte[].class, new JsonSerializer<byte[]>() {

			@Override
			public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
				return new JsonPrimitive(Base64.encodeBase64String(src));
			}

		});

		/*
		 * .registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {
		 * 
		 * @Override public byte[] deserialize(JsonElement json, Type arg1,
		 * JsonDeserializationContext arg2) throws JsonParseException { return
		 * json.getAsString().getBytes(); }
		 * 
		 * });
		 */

		return builder;
	}

	public synchronized static Object getObject(String json, Type type) {
		Object obj = newBuilder().create().fromJson(json, type);

		return obj;
	}

	public synchronized static String getJsonTree(Object obj, Type type) {
		JsonElement je = newBuilder().create().toJsonTree(obj, type);

		return je.toString();
	}

}